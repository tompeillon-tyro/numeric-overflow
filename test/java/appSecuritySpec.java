package app;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    @Ignore
    public void BiggerThanIntMaxSizeNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(new Amount(BigInteger.valueOf(2147483647L)).plus(new Amount(BigInteger.ONE)));
        assertTrue(res,() -> "Bigger than int max size needs approval");
    }

    @Test
    @Ignore
    public void LessThanIntMinSizeNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(new Amount(BigInteger.valueOf(-2147483648L)).plus(new Amount(BigInteger.valueOf(-1))));
        assertTrue(res,() -> "Less than int min size needs approval");
    }
}
