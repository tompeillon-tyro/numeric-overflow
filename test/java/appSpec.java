package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void tooBigAmountThrowsException() {
        Assertions.assertThrows(RuntimeException.class, () -> new Amount(BigInteger.valueOf(100000001L)));
    }

    @Test
    public void tooSmallAmountThrowsException() {
        Assertions.assertThrows(RuntimeException.class, () -> new Amount(BigInteger.valueOf(0)));
    }

    @Test
    public void ThousandsNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(new Amount(BigInteger.valueOf(1000L)));
        assertTrue(res,() -> "1000 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        Main app = new Main();
        boolean res = app.approval(new Amount(BigInteger.valueOf(500L)));
        assertFalse(res,() -> "500 does not need approval");
    }

}
