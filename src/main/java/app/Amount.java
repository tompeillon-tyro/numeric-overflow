package app;

import java.math.BigInteger;

public class Amount {
    private final int value;

    public Amount(BigInteger value) {
        if (value.compareTo(new BigInteger("10000000")) > 0) {
            throw new RuntimeException("Too big");
        }
        if (value.compareTo(BigInteger.ZERO) <= 0) {
            throw new RuntimeException("Too small");
        }
        this.value = value.intValue();
    }

    public int getValue() {
        return value;
    }

    public Amount plus(Amount amount) {
        return new Amount(BigInteger.valueOf(this.value).add(BigInteger.valueOf(amount.value)));
    }
}
