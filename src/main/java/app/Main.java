package app;

import java.math.BigInteger;

public class Main {

    private int threshold = 1000;

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = new Amount(new BigInteger("999"));
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = new Amount(new BigInteger("2000"));
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(Amount amount){
        return amount.getValue() >= threshold;
    }

}
